<div align="center">
  <h1>Ingeniería Social</h1>
</div>

# Tabla de contenido
- [Introduccion](#Introduccion)
    - [Antecedentes de la ingeniería social](#Antecedentes-de-la-ingeniería-social)
    - [¿Qué es la ingeniería social?](#¿Qué-es-la-ingeniería-social?)
    - [¿Por qué funciona la ingeniería social?](#¿Por-qué-funciona-la-ingeniería-social?)
    - [Datos de la ingeniería social](#Datos-de-la-ingeniería-social)
    - [Metas de la ingeniería social](#Metas-de-la-ingeniería-social)
- [Principios de la ingeniería social](#Principios-de-la-ingeniería-social)
    - [Principios de la ingeniería social según Kevin Mitnick y Robert Cialdini](#Principios-de-la-ingeniería-social-según-Kevin-Mitnick-y-Robert-Cialdini)
    - [Principios de la ingeniería social a detalle](#Principios-de-la-ingeniería-social-a-detalle)
    - [Perfil del ingeniero(a) social](#Perfil-del-ingeniero-a-social)
- [Tipos de ingeniería social](#Tipos-de-ingeniería-social)
    - [Tipos de ingeniería social: basada en humanos](#Tipos-de-ingeniería-social-basada-en-humanos)
    - [Tipos de ingeniería social: basada en computadoras](#Tipos-de-ingeniería-social-basada-en-computadoras)
    - [Taxonomía de la ingeniería social: marco de ataque](#Taxonomía-de-la-ingeniería-social-marco-de-ataque)

# <a name="Introduccion">Introduccion</a>
## <a name="Antecedentes-de-la-ingeniería-social">Antecedentes de la ingeniería social</a>

El primer encuentro donde se encuentra una estructura de la Ingeniería Social fue en La Biblia, específicamente en el Genesis, en este escenario el Ingeniero Social fue la serpiente, la cual engaña a Eva para comer del fruto del bien y del mal a través de persuasión.

Otro suceso en el que aparece un ataque de Ingeniería Social, es la historia del caballo de Troya, en donde los soldados de Grecia para poder entrar a Troya, tuvieron que crear un caballo de madera hueco, pero el acontecimiento importante fue del espía que infiltraron para que persuadiera a los troyanos de que aceptaran el caballo como ofrenda de paz y es después de los troyanos festejaran y bebieran los soldados de Grecia hayan salido del caballo a pelear e invadir sin que nadie se hubiese percatado de esto.

- [spotify](https://open.spotify.com/show/7M8pc8E8ElbJ8LMoFi9kFa)
- [defcon](https://www.defcon.org/)
- [Frank_Abagnale_Jr](https://es.wikipedia.org/wiki/Frank_Abagnale_Jr.)

## <a name="¿Qué-es-la-ingeniería-social?">¿Qué es la ingeniería social?</a>

| Concepto | Difinicion |
| ------ | ------ |
| Visentini (2006) | Es una disciplina que consiste en sacar datos corporativos a otra persona, sin que ésta se dé cuenta de que está revelando Información sensible, y que normalmente no lo haría. |
| Borghello (2009) | La ingeniería social puede definirse como una acción o conducta social destinada a conseguir información de las personas cercanas a un sistema. |
| ¿Que es la Ingenieria Social? | Se centra en lograr la confianza de las personas para luego engañarlas y manipularlas para el beneficio propio de quien la implementa. |

#### ¿Qué puedo hacer con un poco de información?

<div align="center">
  <img src="img/informacion.png" width="500">
</div>

#### Ciclo de vida de la ingeniería social

<div align="center">
  <img src="img/informacion1.png" width="500">
</div>

#### Reglas de compromiso

<div align="center">
  <img src="img/informacion2.png" width="500">
</div>

## <a name="¿Por-qué-funciona-la-ingeniería-social?">¿Por qué funciona la ingeniería social?</a>

#### Para que el ataque de ingeniería social funcione(exito), es necesario:

- Ciencia del computo
- Psicología Social.

#### Para que la ingeniería social sea efectiva, se necesita de:

- ###### La disposición humana a confiar

La ingeniería social y la disposición humana a confiar están estrechamente relacionadas, ya que los ingenieros sociales utilizan la confianza como una herramienta clave para manipular a las personas y obtener información confidencial o realizar acciones específicas.

Es cierto que los seres humanos tienen una tendencia natural a confiar en otros, especialmente en aquellos que perciben como autoridades, expertos o personas cercanas a ellos. Los ingenieros sociales se aprovechan de esta tendencia para crear situaciones que hagan que las personas confíen en ellos, ya sea a través de la manipulación emocional, la persuasión o el engaño.

Por ejemplo, un ingeniero social puede hacerse pasar por un empleado de una empresa legítima y solicitar información confidencial a través del teléfono o del correo electrónico. También pueden utilizar técnicas de phishing para engañar a las personas para que revelen información personal o financiera.

Es importante destacar que la confianza no se obtiene de manera automática o fácil. La construcción de la confianza requiere tiempo, y es necesario establecer una relación de respeto y honestidad entre las partes involucradas. Por lo tanto, es fundamental que las personas sean críticas y cuidadosas en cuanto a quiénes les piden información confidencial y en qué contexto lo hacen.

En conclusión, la ingeniería social y la disposición humana a confiar están relacionadas, y los ingenieros sociales utilizan la confianza como una herramienta clave para manipular a las personas y obtener información confidencial o realizar acciones específicas. Es importante que las personas sean críticas y cuidadosas en cuanto a quiénes les piden información confidencial y en qué contexto lo hacen, para evitar ser víctimas de la ingeniería social.

- ###### La disposición humana a la distracción

La ingeniería social y la tendencia del ser humano a la distracción son dos factores estrechamente relacionados, ya que los ingenieros sociales utilizan la distracción como una herramienta clave para manipular a las personas y obtener información confidencial o realizar acciones específicas. En la era digital en la que vivimos, las notificaciones, los correos electrónicos y las redes sociales pueden interrumpir constantemente nuestras actividades, lo que puede hacer que las personas sean más propensas a caer en trampas o revelar información confidencial.

Los ingenieros sociales utilizan técnicas de distracción para engañar a las personas para que revelen información confidencial, como hacer una pregunta aparentemente inocente mientras revisan el teléfono o la computadora de la persona. También pueden utilizar técnicas más sofisticadas, como el uso de dispositivos electrónicos, para distraer a las personas mientras realizan una actividad crítica.

Es importante destacar que la distracción no es algo que se pueda evitar completamente, pero es fundamental que las personas estén conscientes de las técnicas de distracción utilizadas por los ingenieros sociales y estén alertas para evitar caer en sus trampas. Además, es importante que las personas estén siempre atentas a sus dispositivos electrónicos y las notificaciones que reciben, para evitar distracciones innecesarias que puedan llevar a situaciones de riesgo.

En conclusión, es esencial que las personas conozcan las técnicas de distracción utilizadas por los ingenieros sociales y estén siempre alertas a las notificaciones y distracciones innecesarias en sus dispositivos electrónicos. La conciencia y la vigilancia son la clave para evitar caer en trampas y proteger la información confidencial.

- ###### La disposición humana a la atención selectiva

La ingeniería social y la disposición humana a la atención selectiva están estrechamente relacionadas, ya que los ingenieros sociales utilizan la atención selectiva como una herramienta clave para manipular a las personas y obtener información confidencial o realizar acciones específicas.

La atención selectiva es la capacidad del cerebro humano para concentrarse en ciertos estímulos mientras ignora otros. Los ingenieros sociales se aprovechan de esta capacidad para crear situaciones que atraigan la atención de las personas hacia algo específico y las alejen de otros estímulos o situaciones importantes.

Por ejemplo, un ingeniero social puede utilizar técnicas de atención selectiva para distraer a una persona mientras roba información confidencial de su computadora o dispositivo móvil. También pueden utilizar técnicas más sofisticadas, como el uso de dispositivos electrónicos para atraer la atención de una persona hacia algo específico mientras realizan una actividad crítica.

Es importante destacar que la atención selectiva no es algo que se pueda evitar completamente, ya que es una parte natural del funcionamiento del cerebro humano. Sin embargo, es fundamental que las personas sean conscientes de las técnicas de atención selectiva utilizadas por los ingenieros sociales y estén alerta para evitar caer en sus trampas.

Además, es importante que las personas estén siempre atentas a su entorno y a los estímulos que les rodean para evitar ser víctimas de la ingeniería social. Por ejemplo, es importante prestar atención a las personas que nos rodean y a las situaciones en las que nos encontramos, en lugar de distraernos con nuestros dispositivos electrónicos o nuestras propias preocupaciones.

En conclusión, la ingeniería social y la disposición humana a la atención selectiva están relacionadas, y los ingenieros sociales utilizan la atención selectiva como una herramienta clave para manipular a las personas y obtener información confidencial o realizar acciones específicas. Es importante que las personas estén conscientes de estas técnicas de atención selectiva y estén siempre atentas a su entorno para evitar caer en trampas y proteger su información confidencial.

## <a name="Datos-de-la-ingeniería-social">Datos de la ingeniería social</a>

| Datos de la ingeniería social |
| ------ |
| El 80% de los ataques informáticos se deben a errores relacionados con el factor humano y no a temas especificos de tecnología. |
| Los incidentes de seguridad de la información son a menudo causados por fallas humanas (Chan, Woon y Kankahall, 2005) en lugar de fallas técnicas (Schneier, 2000). |
| La ingeniería social es la técnica más eficaz para hacerse con los secretos celosamente protegidos, ya que no requiere de una sólida formación técnica, ni de conocimientos sobre protocolos de y sistemas operativos. |
| Quienes practican la Ingeniería social requieren solamente de astucia, paciencia y una buena dosis de psicología. |
| La ingeniería social constituye un riesgo de seguridad porque se puede utilizar para eludir los sistemas de detección de intrusos, firewalls y sistemas de control de acceso. |
| En la mayoría de los casos las personas (objetivos) no se dan cuenta de que están siendo víctimas de ataques de ingeniería social. (FBI, 2013; Hadnagy y Wilson, 2010). |
| Debemos de tener mucha PACIENCIA, ASTUCIA Y MALICIA. |

## <a name="Metas-de-la-ingeniería-social">Metas de la ingeniería social</a>

#### Motivos:

- Curiosidad
- Venganza
- Beneficio personal o económico
- Diversión
- Desafío
- Mucho más

#### Metas de ingeniería social:

- Las personas son engañadas para que revelen información confidencial como datos bancarios, contraseñas, etc.
- Está información será usada por los delincuentes para estafar, realizar compras a nombre de otro, enviar spam, etc.
- Acceso a información
- Autorización
- Confianza
- Dinero
- Reputación
- Cometer fraude
- Espionaje industrial
- Robo de identidad
- Irrumpir en los sistemas/redes

#### Personal vulnerable a ataques de ingeniería social en una empresa

- Recepcionistas
- Vendedores
- Personal de nómina
- Recursos humanos
- Personal de finanzas
- Administración de oficinas

#### Posiciones atractivas para un ingeniero social

- Departamento de recursos humanos. Información de empleados:
    - Estado actual: trabajando, de vacaciones, enfermo, de momento trabajando en un proyecto fuera de la empresa, etc.
    - Departamento del empleado.
    - Nombre de los colegas.
    - Superiores (si el ingeniero social quiere tomar la identidad de uno de ellos en un ataque).
    - Condición laboral.
    - Información sobre el contrato y salario de un empleado.

#### Personal Extremadamente vulnerable

- Gerenciales
- Newbies
- Temporeros
- Freelancers
- Help Desk Responsabilidades:
    - Cuentas de usuarios (Creación, eliminación, activación, desactivación)
    - Cambiar contraseñas
    - Instalar software (por razones de seguridad los empleados no deben tener permiso para hacer esto)
    - Ofrecer ayuda

#### Cómo escoger un buen objetivo

- Trabaje en áreas donde tenga mucho contacto con el público
- Sea empleada en una compaía asociada con el objetivo
- Sea familiar/amigo del objetivo
- Cuente con amplia presencia en redes sociales
- Que exhiba la característica de ser muy sociable

# <a name="Principios-de-la-ingeniería-social">Principios de la ingeniería social</a>
## <a name="Principios-de-la-ingeniería-social-según-Kevin-Mitnick-y-Robert-Cialdini">Principios de la ingeniería social según Kevin Mitnick y Robert Cialdini</a>

#### Principios de Kevin Mitnick

- Todos los seres humanos quieren ayudar.
- El primer movimiento siempre de confianza hacia el otro.
- No nos gusta decir no.
- A todos nos gusta que nos alaben.
- La ingeniería social utiliza la influencia y la persuasión para engañar a la gente.
- El ingeniero social es capaz de aprovecharse de la gente para obtener informacion con o sin el uso de la tecnología.
- Usted puede tener la mejor tecnología, firewalls, sistemas de detección de ataques, dispositivos biométricos etc.
- Lo único que se necesita es un llamado a una empleado desprevenido. Tienen todo en sus manos.

#### Robert Cialdini

- Conocido mundialmente como el experto en la ciencia de la persuasión y como aplicarla éticamente en los negocios.
- Source: https://bit.ly/38YLgL8
- Ha pasado toda su carrera realizando investigaciones científicas sobre lo que lleva a las persona a decir “Si” cuando se les solicita algo.
- Toda su investigación se fundamenta en los 6 principios de la persuasión.

#### Los 6 principios de persuasión de Cialdini

- 1.El principio de escasez (Ofertas de tiempo limitado).
- 2.El principio de prueba social (Ser parte de algo y ser aceptados).
- 3.El principio de autoridad (Tener mayor autoridad = tener más influencia).
- 4.El principio de simpatía (La simpatía induce a la confianza).
- 5.El principio de consistencia y compromiso (Sentido de compromiso(Empezar algo y seguir hasta el final))
- 6.El principio de reciprocidad (Necesidad de devolver el favor(Me das yo te doy)(Tu me isite algo yo te voy hacer algo))

#### Datos sobre los principios de persuasión de Cialdini

- La persuasión es propiedad del atacante, NO de la víctima.
- Para que los principios de persuasión sean efectivos, debe darse en conjunto con un escenario bien planificado y teniendo conocimiento sobre la víctima.
- Se debe utilizar una combinación de principios para ser efectivos
- Escenario + Ataque + Aplicación de principios = Éxito
- Entre más principios de persuasión se implementen existe más % de éxito.

## <a name="Principios-de-la-ingeniería-social-a-detalle">Principios de la ingeniería social a detalle</a>

**Reciprocidad**

- Cuando una persona nos ofrece algo, tendemos a ofrecerle algo también.
- Por lo contrario, si esa persona no nos trata con respeto, estaremos más susceptibles a pagarle con la misma moneda

> La reciprocidad es un instinto social fácilmente manipulable

**Urgencia y escasez**

- Es un clásico entre los clásicos
- La mayoría de los ataques de ingeniería social consiguen que las personas caigan a través de la urgencia
- Todo aquello que es escaso tiene más valor.
- Es irracional. Sólo hace falta ver cómo reacciona la gente durante las rebajas o antes las ofertas limitadas.

> Esta oferta solo durara durante los procimos cinco minutos.

> Alguien ha haccedido a tu cuenta bancaria, entra en el siguiente enlasce para solucionarlo y proteger tu dinero.

**Consistencia**

- Empezando por pequeñas acciones y continuando por otras más delicadas
- A pesar de que una de esas tareas pueda parecer rara, al haberse comprometido, la llevará a cabo junto al resto.
De este modo el ingeniero social consigue manipular por consistencia

**Simpatía**

Nuestra desconfianza se reduce cuando el interlocutor con el que hablamos nos cae bien o está alineado con nuestros intereses o valores.
Estamos más predispuestos a dejarnos influir por personas que nos agradan, y menos por personas que nos producen rechazo.

**Autoridad**

- El uso de la autoridad juega un papel clave en la usurpación de identidad, ya sea de forma real (robo del perfil digital de una autoridad, por ejemplo un Director) o ficticia (clonado de perfiles o phishing)
- Cunado una persona en practicas de una empresa pide las credenciales de acceso de un servicio, lo mas probable es que sea visto con desconfianza.
- No obtante, si las mismas credenciales son pedidas por un director/a, la situacion cambia.

**Validación social**

- Si recibimos un correo electrónico en el que se nos pide hacer una determinada acción, la cual es extraña, lo más seguro es que pensemos si llevarla a cabo o no. Pero oyes que todo el mundo lo hace la victima llevara acabo la accion
- Sin envargo, si en una misma conversacion hay varios conosidos como por ejemplo compañeros de la misma empresa, y ninguno de ellos pone objecion alguna, lo mas probable es que no acatemos las normas, aun sin saber de donde ni de quien provienen.
- Al ser personas tan gregarias y en búsqueda permanente de la acción social, el sesgo de grupo es continuamente utilizado, también conocido como “presión grupal”, especialmente en el ámbito de la política para movilizar el voto.
- Leer el libro que esta en la carpeta de notas originales


## <a name="Perfil-del-ingeniero-a-social">Perfil del ingeniero(a) social</a>

**Cualidades**

- Capacidad de socializar con facilidad
- Habilidad en el hablar
- Habilidad en el arte de persuasión
- Sonar convincente(Sentirse seguro de si mismo y relagado al imitar o querer imitar un personaje(personaje apuesto, seguro y decidido)) Cuando se crea un persona se deberia tratar que el personaje tenga algunas caracteristicas o componentes familiares que a mi propi yo me interesa o gusten, para estar convencido y sonar convencido. Al crear un personaje con la edad, sexo y la religion dandole identidad a ese personaje, basandonos a la informacion recolectada sobre la victima.(Que es lo que yo quiero lograr).
- Aparente ser inofensivo
- Mantener un perfil bajo
- Sonreír siempre
- Tono de voz cómodo

**Cómo identificarlo?**

- Identificar un ingeniero social puede ser bastante difícil.
- Sin embargo hay personas que al lado de hackers y crackers, tienen un buen potencial para desarrollar un ataque de ingeniería social por su experiencia de trabajo.

**Quién puede atacar y ser un ingeniero(a) social?**

- Ex-empleados molestos
- Infiltrados
- Empleados descontentos
- Visitantes

**Los siguientes puntos facilitan la identificación de un ingeniero social:**

- Intentan presionar a sus víctimas mediante :
  - “Name-dropping”
  - Uso de autoridad o amenazas
- Presionan creando un sentido de gratitud en las víctimas (reciprocidad), siendo amable y cordial

> “Una víctima debe esta muy atenta a todo lo que se pregunta, independientemente de lo que se dijo antes o después, sin tener miedo por preguntar su derecho a saber una información delicada.”

> Como Ingeniro Social y cunado se asumamos la Posiciones de ingeniro social, no debemos dejarnos precionar , nosotros somos los que debemos de precionar.

- Favorecen la sensación de comodidad de la víctima.
- Provocan que la víctima contravenga derechos y políticas de seguridad, obteniendo la información deseada
- Aparentan ser confiables a otros:
- Incluyen información que han conseguido antes, mientras atacan a otra persona.

# <a name="Tipos-de-ingeniería-social">Tipos de ingeniería social</a>

## <a name="Tipos-de-ingeniería-social-basada-en-humanos">Tipos de ingeniería social: basada en humanos</a>

**Basada en humanos**

- consiste en recolectar información sensible mediante la interacción entre humanos

**Basada en Tecnología**

- Se lleca a cabo con la ayuda de computadoras

**Estrategia basada en Humanos**

- Imitando se un usuario legítimo.
- Imitando ser una persona importante (alto rango).
- Imitando ser personal técnico.
- Espiar por encima de su hombro (shoulder surfing).
- “Dumpster diving”- Buscando en los depositos de basura.
- Buscando en los contenedores de basura.
  - Depende en el pais donde estemos, puede ser ilegal/legal. Al buzcar en la basura.
  - Recibos de facturas, luz, agua, teléfono, cable u otros servicios.
  - Información financiera.
- Revisión de desperdicios o basura
  - Manuales de operación de sistemas.
  - Reportes con información.
  - Cuentas de usuarios y sus contraseñas.
  - Formatos con menbretes.
  - Papel con sellos.
- Contramedidas para el Dumpster Diving
  - Candado en los contenedores.
  - Colocar portones.
  - Colocar letreros “No pase”.
  - Luces.
  - Utilizar cámaras de seguridad.
  - Tritutar documentos.
  - Utilizar servicios de recolección.

**En tu casa como botas tu la basura**

> Cuando tiro información sensible a la basura la destruyo y luego la tiro por partes, un día tiro una parte y a los 3 días tiro el resto.

## <a name="Tipos-de-ingeniería-social-basada-en-computadoras">Tipos de ingeniería social: basada en computadoras</a>

**Estrategias basas en tecnología**

- Email con malware
- Spam (Correo no deseado)
- Cadena de cartas (Chain letters)
- Emails de engaño (hoaxes)
- “Phishing”
- Instalando un keylogger (software(Ejecutable) || hardware (Rubber Ducky))

## <a name="Taxonomía-de-la-ingeniería-social-marco-de-ataque">Taxonomía de la ingeniería social: marco de ataque</a>

<div align="center">
  <img src="img/Taxonomia.png" width="800">
</div>

- [Ingeniería Social: el arte de la manipulación humana](https://tahiridikovec.com/ingenieria-social-el-arte-de-la-manipulacion-humana/)








- [holaholaohlaohlah](#holaholaholahola)

# <a name="holholaholaohlah">holaholaholaholahola</a>

```sh
```
