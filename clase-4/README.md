<div align="center">
  <h1>Acerca de phishing</h1>
</div>

# ¿Qué es phishing?

Phishing es el delito de engañar a las personas para que compartan información confidencial como contraseñas y números de tarjetas de crédito. Como ocurre en la pesca, existe más de una forma de atrapar a una víctima, pero hay una táctica de phishing que es la más común. Las víctimas reciben un mensaje de correo electrónico o un mensaje de texto que imita (o “suplanta su identidad”) a una persona u organización de confianza, como un compañero de trabajo, un banco o una oficina gubernamental. Cuando la víctima abre el correo electrónico o el mensaje de texto, encuentra un mensaje pensado para asustarle, con la intención de debilitar su buen juicio al infundirle miedo. El mensaje exige que la víctima vaya a un sitio web y actúe de inmediato o tendrá que afrontar alguna consecuencia.

Si un usuario pica el anzuelo y hace clic en el enlace, se le envía a un sitio web que es una imitación del legítimo. A partir de aquí, se le pide que se registre con sus credenciales de nombre de usuario y contraseña. Si es lo suficientemente ingenuo y lo hace, la información de inicio de sesión llega al atacante, que la utiliza para robar identidades, saquear cuentas bancarias, y vender información personal en el mercado negro.

<div align="center">
  <h1>“El phishing es la forma más sencilla de ciberataque y, al mismo tiempo, la más peligrosa y efectiva”</h1>
</div>

A diferencia de otros tipos de amenazas de Internet, el phishing no requiere conocimientos técnicos especialmente sofisticados. De hecho, según Adam Kujawa, Director de Malwarebytes Labs, “el phishing es la forma más sencilla de ciberataque y, al mismo tiempo, la más peligrosa y efectiva. Eso se debe a que ataca el ordenador más vulnerable y potente del planeta: la mente humana”. Los autores del phishing no tratan de explotar una vulnerabilidad técnica en el sistema operativo de su dispositivo, sino que utilizan “ingeniería social”. Desde Windows e iPhones a Macs y Androids, ningún sistema operativo está completamente a salvo del phishing, con independencia de lo sólida que sea su seguridad. De hecho, los atacantes a menudo recurren al phishing porque no pueden encontrar ninguna vulnerabilidad técnica. ¿Por qué perder el tiempo tratando de burlar capas de seguridad cuando puede engañar a alguien para que le entregue la llave? En la mayoría de los casos, el eslabón más débil en un sistema de seguridad no es un fallo oculto en el código informático, sino una persona que no comprueba la procedencia de un correo electrónico.


# Historia del phishing

El origen del nombre “phishing” es fácil de rastrear. El proceso de llevar a cabo una estafa de phishing es muy similar al de la pesca (“fishing” en inglés). Se prepara el anzuelo pensando en engañar a una víctima, y luego se lanza y se espera a que pique. En cuanto al dígrafo “ph” en sustitución de “f,” podría ser el resultado de la combinación de las palabras inglesas “fishing” y “phony,” pero algunas fuentes apuntan a otro posible origen.

En los años 70, se formó una subcultura en torno a los ataques de baja tecnología para explotar el sistema telefónico. Estos primeros hackers se llamaban “phreaks”, una combinación de las palabras inglesas “phone” (teléfono) y “freak” (raro, friqui). En una época en la que no había demasiados ordenadores en red que hackear, el phreaking era una forma común de hacer llamadas gratuitas de larga distancia o llegar a números que no salían en los listines.


<div align="center">
  <h1>“El phishing es la forma más sencilla de ciberataque y, al mismo tiempo, la más peligrosa y efectiva”</h1>
</div>

Incluso antes de que arraigara el término “phishing”, se describió en detalle una técnica de phishing en una presentación del Grupo Internacional de Usuarios HP, Interex, en 1987.

La creación del término se atribuyó a un conocido spammer y hacker de mediados de los años 90, Khan C Smith. Asimismo, según los registros de Internet, la primera vez que se utilizó públicamente la palabra phishing y quedó registrado fue el 2 de enero de 1996. La mención ocurrió en un grupo de noticias Usenet denominado AOHell. En ese momento, America Online (AOL) era el proveedor número uno de acceso a Internet, con millones de conexiones diarias.

Naturalmente, la popularidad de AOL la convirtió en blanco de los estafadores. Los hackers y piratas informáticos la utilizaron para comunicarse entre sí, así como para realizar ataques de phishing contra usuarios legítimos. Cuando AOL adoptó medidas para cerrar AOHell, los atacantes recurrieron a otras técnicas. Enviaban mensajes a los usuarios de AOL afirmando ser empleados de esta compañía y les pedían que verificaran sus cuentas y facilitaran la información de facturación. Con el tiempo, el problema creció tanto que AOL añadió advertencias en todos los programas cliente de correo electrónico y mensajería instantánea indicando que “nadie que trabaje en AOL le pedirá su contraseña o información de facturación”.


<div align="center">
  <h1>“Los sitios de redes sociales se convirtieron en un objetivo principal de phishing”</h1>
</div>


En la década de 2000, el phishing dirigió su atención a explotar los sistemas de pago online. Se hizo común que los phishers dirigieran sus ataques a los clientes de servicios de pago bancario y online, algunos de los cuales, según investigaciones posteriores, fueron identificados correctamente y asociados al banco que verdaderamente utilizaban. De igual forma, los sitios de redes sociales se convirtieron en un objetivo principal del phishing, que era atractivo para los defraudadores porque los detalles personales registrados en dichos sitios son de utilidad para el robo de identidad.

Los delincuentes registraron docenas de dominios que se hacían pasar por eBay y PayPal imitándolos tan bien que parecían reales si no se prestaba la suficiente atención. Los clientes de PayPal recibieron entonces correos electrónicos de phishing (con enlaces al sitio web falso), pidiéndoles que actualicen los números de su tarjeta de crédito y otra información personal. The Banker (una publicación propiedad de The Financial Times Ltd.) informó del primer ataque conocido de phishing contra un banco en septiembre de 2003.

A mediados de la década de 2000, un software “llave en mano” de phishing estaba disponible en el mercado negro. Al mismo tiempo, grupos de hackers empezaron a organizarse para elaborar sofisticadas campañas de phishing. Las estimaciones de pérdidas debido al éxito de los ataques de phishing durante este período varían, con un informe de Gartner de 2007 que indica que 3,6 millones de adultos perdieron 3.200 millones de dólares entre agosto de 2006 y agosto de 2007.

<div align="center">
  <h1>“En 2013, se robaron 110 millones de registros de clientes y tarjetas de crédito de los clientes de Target”</h1>
</div>


En 2011, el phishing encontró patrocinadores estatales cuando una presunta campaña china de phishing atacó cuentas de Gmail de altos cargos políticos y mandos militares de Estados Unidos y Corea del Sur, así como de activistas políticos chinos.

En 2013, en el evento posiblemente más famoso, se robaron 110 millones de registros de clientes y tarjetas de crédito de los clientes de Target, por medio de la cuenta de un subcontratista suplantada con phishing.

Incluso más infame fue la campaña de phishing lanzada por Fancy Bear (un grupo de ciberespionaje asociado con el Departamento Central de Inteligencia ruso GRU) contra las direcciones de correo electrónico asociadas con el Comité Nacional Demócrata en el primer trimestre de 2016. En particular, al director de la campaña de Hillary Clinton en las elecciones presidenciales de 2016, John Podesta, se le hackeo su Gmail, con las filtraciones subsiguientes, después de caer en el truco más antiguo: un ataque de phishing que afirmaba que su contraseña de correo electrónico se había visto comprometida (por lo tanto, haga clic aquí para cambiarla).

En 2017, una estafa masiva de phishing engañó a los departamentos de contabilidad de Google y Facebook para que transfirieran dinero, un total de más de 100 millones de dólares, a cuentas bancarias en el extranjero bajo el control de un hacker.


## Tipos de ataques de phishing

A pesar de sus muchas variedades, el denominador común de todos los ataques de phishing es el uso de un pretexto fraudulento para adquirir datos valiosos. Algunas categorías principales incluyen:

## Spear phishing

Mientras la mayoría de las campañas de phishing envían correos electrónicos masivos al mayor número posible de personas, el spear phishing es un ataque dirigido. Spear phishing ataca a una persona u organización específica, a menudo con contenido personalizado para la víctima o víctimas. Requiere un reconocimiento previo al ataque para descubrir nombres, cargos, direcciones de correo electrónico y similares. Los hackers buscan en Internet para relacionar esta información con lo que han averiguado sobre los colegas profesionales del objetivo, junto con los nombres y las relaciones profesionales de los empleados clave en sus organizaciones. Con esto, el autor del phishing crea un correo electrónico creíble.

Por ejemplo, un estafador podría crear un ataque de spear phishing a un empleado cuyas responsabilidades incluyen la capacidad de autorizar pagos. El correo electrónico aparenta proceder de un ejecutivo en la organización, que exige al empleado que envíe un pago sustancial al ejecutivo o a un proveedor de la empresa (cuando en realidad el enlace del pago malicioso lo envía al atacante).

Spear phishing es una amenaza crítica para empresas (y gobiernos), y cuesta mucho dinero. Según un informe de 2016 de una investigación sobre el tema, el spear phishing fue responsable del 38% de los ciberataques en las empresas participantes durante 2015. Además, para las empresas estadounidenses implicadas, el coste medio de los ataques de spear phishing fue de 1,8 millones de dólares por incidente.

<div align="center">
  <h1>“Un extenso correo electrónico de phishing de alguien que afirmaba ser un príncipe nigeriano es una de las estafas más antiguas de Internet”</h1>
</div>


### Phishing de clonación

En este ataque, los delincuentes hacen una copia, o clonan, correos electrónicos legítimos enviados anteriormente que contienen un enlace o un archivo adjunto. Luego, el autor del phishing sustituye los enlaces o archivos adjuntos con contenido malicioso disfrazado para hacerse pasar por el auténtico. Los usuarios desprevenidos hacen clic en el enlace o abren el adjunto, lo que a menudo permite tomar el control de sus sistemas. Luego el autor del phishing puede falsificar la identidad de la víctima para hacerse pasar por un remitente de confianza ante otras víctimas de la misma organización.

### 419/Estafas nigerianas


Un extenso correo electrónico de phishing de alguien que afirmaba ser un príncipe nigeriano es una de las estafas más antiguas de Internet. Según Wendy Zamora, jefe de contenido de Malwarebytes Labs, “el phishing del príncipe nigeriano procede de una persona que afirma ser un funcionario del gobierno o miembro de una familia real que necesita ayuda para transferir millones de dólares desde Nigeria. El correo electrónico se marca como 'urgente' o 'privado' y su remitente solicita al destinatario que proporcione un número de cuenta bancaria para remitir los fondos a un lugar seguro”.

En una divertida actualización de la clásica plantilla del phishing nigeriano, el sitio web británico de noticias Anorak informó en 2016 de que había recibido un mensaje de correo electrónico de un tal Dr. Bakare Tunde, que afirmaba ser el director del proyecto de Astronáutica de la Agencia Nacional de Investigación y Desarrollo Espacial de Nigeria. El Dr. Tunde afirmaba que su primo, el comandante de las Fuerzas Aéreas Abacha Tunde, se había quedado atrapado en una antigua estación espacial soviética durante más de 25 años. Pero, por sólo 3 millones de dólares, las autoridades espaciales rusas podrían organizar un vuelo para llevarle a casa. Todo lo que los destinatarios tenían que hacer era enviar la información de su cuenta bancaria para transferir la cantidad necesaria, y el Dr. Tunde les pagaría una comisión de 600.000 dólares.

Por cierto, el número “419” está asociado con esta estafa. Hace referencia a la sección del código penal nigeriano que trata sobre fraude, los cargos y las penas para los infractores.

### Phishing telefónico

Con los intentos de phishing a través del teléfono, a veces llamados phishing de voz o “vishing,” el phisher llama afirmando representar a su banco local, la policía o incluso la Agencia Tributaria. A continuación, le asustan con algún tipo de problema e insisten en que lo solucione inmediatamente facilitando su información de cuenta o pagando una multa. Normalmente le piden que pague con una transferencia bancaria o con tarjetas prepago, porque son imposibles de rastrear.

Phishing vía SMS, o “smishing,” es el gemelo malvado del vishing, que realiza el mismo tipo de estafa (algunas veces con un enlace malicioso incorporado en el que hacer clic) por medio de un mensaje de texto SMS.

<div align="center">
  <h1>“El correo electrónico hace una oferta que parece demasiado buena para ser verdad”</h1>
</div>



## Cómo identificar un ataque de phishing

Reconocer un intento de phishing no siempre es sencillo, pero algunos consejos, un poco de disciplina y algo de sentido común pueden ayudar mucho. Busque algo que sea raro o inusual. Pregúntese si el mensaje le despierta alguna sospecha. Confíe en su intuición, pero no se deje llevar por el miedo. Los ataques de phishing a menudo utilizan el miedo para nublar su razonamiento.

Aquí tiene algunas señales más de un intento de phishing:

El correo electrónico hace una oferta que parece demasiado buena para ser verdad. Podría decir que ha ganado la lotería, un premio caro, o alguna otra cosa de valor muy elevado.  


- Reconoce al remitente, pero es alguien con quién no trata. Incluso si conoce el nombre del remitente, sospeche si es alguien con quien normalmente no se comunica, especialmente si el contenido del correo electrónico no tiene nada que ver con sus responsabilidades laborales habituales. Lo mismo ocurre si aparece en copia en un correo electrónico a personas a las que ni siquiera conoce, o quizá un grupo de colegas de departamentos con los que no tiene relación.
- El mensaje suena aterrador. Tenga cuidado si el correo electrónico tiene un lenguaje alarmista para crear un **sentido de urgencia**, instándole a que haga clic y “actúe ahora” antes de se elimine su cuenta. Recuerde, las organizaciones responsables no solicitan detalles personales a través de Internet.
- El mensaje contiene archivos adjuntos inesperados o extraños. Estos adjuntos pueden contener malware, ransomware o alguna otra amenaza online.
- El mensaje contiene enlaces que parecen un poco extraños. Incluso si no siente un hormigueo por ninguno de los puntos anteriores, no asuma que los hiperenlaces incluidos llevan a donde parece. En su lugar, pase el cursor por encima del enlace para ver la URL real. Esté especialmente atento a sutiles errores ortográficos en un sitio web que le sea familiar, porque indica una falsificación. Siempre es mejor escribir directamente la URL en lugar de hacer clic en el enlace incorporado.


Aquí tiene un ejemplo de un intento de phishing que suplanta la identidad de un aviso de PayPal, solicitando al destinatario que haga clic en el botón “Confirmar ahora”. Al pasar el cursor del ratón por encima del botón se revela la URL de destino real en el rectángulo rojo.


<div align="center">
  <img src="../img/phishing.jpg" width="1000">
</div>

Aquí tiene otra imagen de un ataque de phishing, esta vez afirmando ser de Amazon. Observe la amenaza de cerrar la cuenta si no hay respuesta en 48 horas.

<div align="center">
  <img src="../img/phishing1.jpg" width="1000">
</div>

Hacer clic en el enlace le lleva a este formulario, que le invita a revelar lo que el phisher necesita para saquear sus bienes:

<div align="center">
  <img src="../img/phishing2.jpg" width="1000">
</div>


## ¿Cómo protegerse del phishing?

Como se ha indicado previamente, el phishing es una amenaza que ofrece “igualdad de oportunidades”, capaz de aparecer en ordenadores de escritorio, portátiles, tabletas y teléfonos inteligentes. La mayoría de los navegadores de Internet disponen de formas de comprobar si un enlace es seguro, pero la primera línea de defensa contra el phishing es su buen criterio. Aprenda a reconocer los signos del phishing e intente practicar informática segura siempre que compruebe su correo electrónico, lea posts de Facebook, o juegue a su juego online favorito.

Una vez más, nuestro Adam Kujawa propone algunas de las prácticas más importantes para mantenerse a salvo:

- No abra correos electrónicos de remitentes que no le sean familiares.
- No haga clic en un enlace dentro de un correo electrónico a menos que sepa exactamente a dónde le lleva.
- Para aplicar esa capa de protección, si recibe un correo electrónico de una fuente de que la que no está seguro, navegue manualmente hasta el enlace proporcionado escribiendo la dirección legítima del sitio web en su navegador.
- Busque el certificado digital del sitio web.
- Si se le pide que proporcione información confidencial, compruebe que la URL de la página comienza con “HTTPS” en lugar de simplemente “HTTP”. La “S” significa “seguro”. No es una garantía de que un sitio sea legítimo, pero la mayoría de los sitios legítimos utilizan HTTPS porque es más seguro. Los sitios HTTP, incluso los legítimos, son vulnerables para los hackers.
- Si sospecha que un correo electrónico no es legítimo, seleccione un nombre o parte del texto del mensaje y llévelo a un motor de búsqueda para ver si existe algún ataque de phishing conocido que utiliza los mismos métodos.
- Pase el cursor del ratón por encima del enlace para ver si es legítimo.


Como siempre, recomendamos utilizar algún tipo de software de seguridad antimalware. La mayoría de las herramientas de seguridad informática tienen la capacidad de detectar cuando un enlace o un archivo adjunto no es lo que parece, por lo que incluso si llega a caer en un intento inteligente de phishing, no terminará compartiendo su información con las personas erróneas.

Todos los productos de seguridad de Malwarebytes Premium proporcionan protección sólida contra el phishing. Pueden detectar sitios fraudulentos e impedir que los abra, incluso si está convencido de que son legítimos.

Por lo tanto, manténgase alerta, y esté atento a cualquier cosa sospechosa.
